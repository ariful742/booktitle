<?php
namespace App\Arif\BanglaBoi\Book;
if(!isset($_SESSION)){
    session_start();
}

class Message{
    static public function message($message=NULL){
        if(is_null($message)){
            $_message=  self::getMessage();
            return $_message;
        }
 else {
            self::setMessage($message);
 }
    }
    static private function getMessage(){
        $_message=$_SESSION['message'];
        $_SESSION['message']="";
        return $_message;
    }
    static private function setMessage($message){
        $_SESSION['message']=$message;
    }
}

