<?php
include_once ('../../../../vendor/autoload.php');
use App\Arif\BanglaBoi\Book\Book;

$books= new Book();
$book=$books->prepare($_GET)->show();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>Show | show single book</title>
    </head>
    <body>
        <h1><?php echo $book->title;?></h1>
        <dl>
            <dt>ID</dt>
            <dd><?php echo $book->id;?></dd>
        </dl>
        <nav>
            <li><a href="index.php">Go to List</a></li>
        </nav>
    </body>
</html>

