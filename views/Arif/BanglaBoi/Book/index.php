<?php
include_once ('../../../../vendor/autoload.php');
session_start();
use App\Arif\BanglaBoi\Book\Book;
use App\Arif\BanglaBoi\Book\Message;

$books= new Book();
$book=$books->index();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>Index | show Book list</title>
        <style>
            .warning{
                background-color:green;
                color:black;
            }
        </style>
    <div class="warning">
        <?php
            if(array_key_exists('message',$_SESSION)&& !empty($_SESSION['message'])){
                echo Message::message();
            }
        ?>
    </div>
    </head>
    <body>
        <h1>LIST OF BOOKS</h1>
        <div>
            <span>Search/Filter Download as <a href="#">PDF</a></span>
            | <a href="#">XL</a> |
            <a href="create.php">Create New</a>
            <select>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
            </select>
        </div>
        <table border="1">
            <thead>
            <th>SL.</th>
            <th>Title</th>
            <th>Action</th>
            </thead>
            <tbody>
                <?php
                $slno=0;
                foreach ($book as $allbook){
                    $slno++; 
            ?>
            <tr>
               <td><?php echo $slno;?></td>
            <td>
              <a href="show.php?id=<?php echo $allbook['id'];?>"><?php echo $allbook['title']; ?></a>
            </td>
            <td>
                <a href="edit.php?id=<?php echo $allbook['id'];?>">Edit</a>
                <a href="delete.php?id=<?php echo $allbook['id'];?>">Delete</a>
                <form action="delete.php" method="post">
                    <input type="hidden" name="id" value="<?php echo $allbook['id']?>"/>
                    <button class="delete" type="submit">Delete2</button>
                </form>
                |<a href="#">Trash</a>/
                Recover|Email to Friend
            </td>
               </tr>
               <?php 
                }
               ?>
            </tbody>
        </table>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function(){
                $(".warning").show().delay(5000).fadeOut('slow');
            });
        </script>
        <script>
                $(document).ready(function(){
                    $('.delete').bind('click',function(e){
                        var isOk= confirm("Are you sure you want to delete?");
                        if(!isOk){
                            e.preventDefault();
                        }
                    });
                });
        </script>
    </body>
</html>

