<?php
include_once ('../../../../vendor/autoload.php');
use App\Arif\BanglaBoi\Book\Book;

$books= new Book();
$book=$books->prepare($_GET)->edit();
?>
<html>
    <head>
        <title>Edit an Item</title>
    </head>
    <body>
        <h1>Edit an item</h1>
        <form action="update.php" method="post">
           <fieldset>
            <legend> Edit Book title</legend>
            <input type="hidden" name="id" value="<?php echo $book->id; ?>"/>
            <div>
               <label for="title">Mobile title</label>
                <input 
                    name="title"
                    id="title"
                    autofocus="true"
                    tabindex="10"
                    placeholder="please enter your mobile name"
                    value="<?php echo $book->title?>"
                    />
                <button type="submit" tabindex="0">Save</button>
                <input type="reset" value="Reset">
            </div>
        </fieldset>
        </form>
        <nav>
            <li><a href="index.php">Go to List</a></li>
        </nav>
    </body>
</html>
